package tomer.whatif.utils;

import android.app.Activity;
import android.content.Intent;
import android.view.inputmethod.InputMethodManager;

import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * Created by Tomer on 04-Nov-15.
 */
public class Utils {

    public enum Action {
        ADD, REMOVE
    }

    public static boolean isNumber(String courseGrade) {
        if (courseGrade == null){
            return false;
        }
        try {
            Integer.parseInt(courseGrade);
            return true;
        } catch (NumberFormatException e) {}
        try {
            Double.parseDouble(courseGrade);
            return true;
        } catch (NumberFormatException e) {}
        return false;
    }

    public static String reverse(String s) {
        return new StringBuilder(s).reverse().toString();
    }

    public static String formatAverage(double average) {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(average);
    }

    public static String formatAverage(String average) {
        return formatAverage(Double.parseDouble(average));
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity == null) {
            return;
        }
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void sendEmail(String[] recipients, String subject, Activity sendingActivity) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.setType("message/rfc822");
        sendingActivity.startActivity(Intent.createChooser(intent, "Choose an Email client: "));
    }
}
