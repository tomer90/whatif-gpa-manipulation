package tomer.whatif.utils;

/**
 * Created by Tomer on 30-Dec-15.
 */
public class ErrorConstants {
    public static final int NO_COURSES_FOUND_ERROR_CODE = -33;
    public static final int WRONG_CREDENTIALS_ERROR_CODE = -2;
    public static final int UNKNOWN_ERROR_ERROR_CODE = -1;
    public static final int UNABLE_TO_REACH_UG_ERROR_CODE = -4;
    public static final String WRONG_CREDENTIALS_ERROR_MESSAGE = "Wrong Username or Password";
    public static final String UNKNOWN_ERROR_MESSAGE = "An error has occurred, check your internet connection";
    public static final String NO_COURSES_FOUND_ERROR_MESSAGE = "Found no courses! Courses will be saved after your first login";
    public static final String UNABLE_TO_REACH_UG_ERROR_MESSAGE = "Unable to reach UG, it might be down";
}
