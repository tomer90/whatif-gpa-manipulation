package tomer.whatif.utils;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import tomer.whatif.courses.Course;

/**
 * Created by Tomer on 04-Jan-16.
 */
public class CoursesDBWrapper {
    public static final String DB_NAME_COL = "name";
    public static final String DB_VALUE_COL = "value";
    public static final String COURSES_TABLE_NAME = "CoursesTable";
    private SQLiteDatabase coursesDB;

    public CoursesDBWrapper(SQLiteDatabase coursesDB) {
        this.coursesDB = coursesDB;
    }

    public List<Course> getCoursesList() {
        ArrayList<Course> courses = new ArrayList<>();
        coursesDB.execSQL("CREATE TABLE IF NOT EXISTS "
                + COURSES_TABLE_NAME
                + " (" + DB_NAME_COL + " VARCHAR, " + DB_VALUE_COL + " VARCHAR);");
        Cursor c = coursesDB.rawQuery("SELECT * FROM " + COURSES_TABLE_NAME
                + " WHERE " + DB_NAME_COL + " LIKE 'Course%'", null);
        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                courses.add(Course.parseFromString(c.getString(c.getColumnIndex(DB_VALUE_COL))));
            } while (c.moveToNext());
        }
        c.close();
        return courses;
    }

    public void storeCoursesList(List<Course> courses) {
        coursesDB.execSQL("DROP TABLE IF EXISTS " + COURSES_TABLE_NAME);
        coursesDB.execSQL("CREATE TABLE "
                + COURSES_TABLE_NAME
                + " (" + DB_NAME_COL + " VARCHAR, " + DB_VALUE_COL + " VARCHAR);");
        for (int i = 0; i < courses.size(); i++) {
            ContentValues contentValues = getContentValues("Course " + String.valueOf(i),
                    courses.get(i).encodeToString());
            coursesDB.insert(COURSES_TABLE_NAME, null, contentValues);
        }
    }

    @NonNull
    private ContentValues getContentValues(String name, String value) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DB_NAME_COL, name);
        contentValues.put(DB_VALUE_COL, value);
        return contentValues;
    }

    @Override
    protected void finalize() throws Throwable {
        coursesDB.close();
        super.finalize();
    }

    public void close() {
        coursesDB.close();
    }
}
