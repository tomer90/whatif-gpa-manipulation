package tomer.whatif.utils;

import java.util.Collection;

import tomer.whatif.courses.Course;
import tomer.whatif.courses.CoursesParser;

/**
 * Created by Tomer on 04-Nov-15.
 */
public class GPAAccumulator {
    double numOfPoints = 0.0;
    double average = 0.0;

    public void addCoursesToAverage(Collection<Course> courses) {
        processCourses(courses, Utils.Action.ADD);
    }

    public void addSingleCourseToAverage(Course course) {
        processSingleCourse(course, Utils.Action.ADD);
    }

    public void removeSingleCourseFromAverage(Course course) {
        processSingleCourse(course, Utils.Action.REMOVE);
    }

    public String getFormattedCurrentAverage() {
        return Utils.formatAverage(average);
    }

    public String getNumOfPointsAsString() {
        return String.valueOf(numOfPoints);
    }

    public double getNumOfPoints() {
        return numOfPoints;
    }

    private void processCourses(Collection<Course> courses, Utils.Action action) {
        if (courses == null || action == null)
            return;
        for (Course c : courses) {
            processSingleCourse(c, action);
        }
    }

    private void processSingleCourse(Course course, Utils.Action action) {
        if (course == null
                || action == null
                || CoursesParser.isSemesterSummaryPlaceHolder(course)
                || !Utils.isNumber(course.getGrade())) {
            return;
        }
        double courseNumOfPoints = Double.parseDouble(course.getNumOfPoints());
        int grade = Integer.parseInt(course.getGrade());
        if (Utils.Action.ADD.equals(action)){
            numOfPoints += courseNumOfPoints;
            average += ((grade - average) * courseNumOfPoints) / numOfPoints;
        } else {
            numOfPoints -= courseNumOfPoints;
            if (numOfPoints == 0) {
                average = 0;
            } else {
                average -= ((grade - average) * courseNumOfPoints) / numOfPoints;
            }
        }
    }
}
