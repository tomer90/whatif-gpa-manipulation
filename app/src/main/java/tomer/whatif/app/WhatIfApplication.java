package tomer.whatif.app;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import tomer.whatif.utils.CoursesDBWrapper;

/**
 * Created by Tomer on 04-Jan-16.
 */
public class WhatIfApplication extends Application {
    public static final String SHARED_PREFS_USERNAME_KEY = "username";
    public static final String SHARED_PREFS_PASSWORD_KEY = "password";

    private static final String COURSES_DB_NAME = "CoursesDB";

    public CoursesDBWrapper getCoursesDBWrapper() {
        return new CoursesDBWrapper(this.openOrCreateDatabase(COURSES_DB_NAME, MODE_APPEND, null));
    }

    public String getStringValueFromSharedPrefs(String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        return prefs.getString(key, null);
    }

    public void storeStringValueInSharedPrefs(String key, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString(key, value);
        prefsEditor.apply();
    }
}
