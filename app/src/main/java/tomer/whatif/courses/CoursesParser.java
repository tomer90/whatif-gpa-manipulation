package tomer.whatif.courses;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tomer.whatif.courses.Course;
import tomer.whatif.utils.Utils;

/**
 * Created by Tomer on 01-Nov-15.
 */
public class CoursesParser {
    public static final String COURSE_NAME_AND_NUM_DELIMITER = "\\s+";
    private static final int COURSE_NUM_LENGTH = 6;
    public static final int COURSE_POINTS_CHILD_INDEX = 1;
    public static final int COURSE_GRADE_CHILD_INDEX = 0;
    public static final int COURSE_NAME_AND_NUM_CHILD_INDEX = 2;

    public ArrayList<Course> parse(String htmlResponseLines) {
        ArrayList<Course> courses = new ArrayList<>();
        Map<String, Integer> sportCoursesRepeatsByCourseNum = new HashMap<>();
        Document parsedHtmlDoc = Jsoup.parse(htmlResponseLines);
        boolean foundGradedCourse = false;
        for (Element e : parsedHtmlDoc.select("TR[ALIGN=RIGHT]")) {
            if (shouldIgnoreElement(e)) {
                continue;
            }
            if (isSemesterSummaryElement(e)) {
                if (foundGradedCourse) {
                    courses.add(new Course(null, null, null, null));
                }
                continue;
            }
            String courseNameAndNum = e.child(COURSE_NAME_AND_NUM_CHILD_INDEX).text();
            String courseNumOfPoints = e.child(COURSE_POINTS_CHILD_INDEX).text();
            String courseGrade = e.child(COURSE_GRADE_CHILD_INDEX).text().replace("*", "").trim();
            foundGradedCourse = foundGradedCourse || isGradeValid(courseGrade);
            Course newCourse = new Course(courseNumOfPoints, courseGrade, getCourseName(courseNameAndNum),
                    getCourseNum(courseNameAndNum));
            if (isSportCourse(newCourse.getName())) {
                String courseNum = newCourse.getNum();
                if (sportCoursesRepeatsByCourseNum.get(courseNum) == null) {
                    sportCoursesRepeatsByCourseNum.put(courseNum, 1);
                } else {
                    String currRepeatNum = String.valueOf(sportCoursesRepeatsByCourseNum.get(courseNum));
                    newCourse.setCourseNum(courseNum + "-" + currRepeatNum);
                    sportCoursesRepeatsByCourseNum.put(courseNum, Integer.parseInt(currRepeatNum) + 1);
                }
            }
            courses.add(newCourse);
        }
        return courses;
    }

    private boolean isSemesterSummaryElement(Element e) {
        return e.hasAttr("BGCOLOR");
    }

    public static boolean isSemesterSummaryPlaceHolder(Course c) {
        return c.getNum() == null
                || c.getNum().length() < COURSE_NUM_LENGTH;
    }

    private boolean isGradeValid(String courseGrade) {
        return "-".equals(courseGrade)
                || Utils.isNumber(courseGrade);
    }

    private boolean shouldIgnoreElement(Element e) {
        return e.children().size() < 3
                || (!isSemesterSummaryElement(e) && e.child(COURSE_POINTS_CHILD_INDEX).text().equals("0.0"))
                || ("םילשה אל").equals(e.child(COURSE_GRADE_CHILD_INDEX).text());
    }

    private String getCourseName(String nameAndCourseNum) {
        StringBuilder nameSB = new StringBuilder();
        String[] nameAndCourseNumArray = nameAndCourseNum.split(COURSE_NAME_AND_NUM_DELIMITER);
        for (int index = nameAndCourseNumArray.length - 2; index >= 0; index--) {
            nameSB.append(Utils.reverse(nameAndCourseNumArray[index]));
            nameSB.append(" ");
        }
        return nameSB.toString();
    }

    private String getCourseNum(String nameAndCourseNum) {
        String[] array = nameAndCourseNum.split(COURSE_NAME_AND_NUM_DELIMITER);
        return array[array.length - 1];
    }

    private boolean isSportCourse(String courseName) {
        return courseName.contains("חינוך גופני")
                || courseName.contains("ספורט")
                || courseName.contains("ג\"ח");
    }
}
