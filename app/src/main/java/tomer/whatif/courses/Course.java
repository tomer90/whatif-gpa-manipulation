package tomer.whatif.courses;

import java.util.UUID;

/**
 * Created by Tomer on 01-Nov-15.
 */
public class Course {
    public static final String ENCODING_DELIM = "&&";
    private String numOfPoints;
    private String grade;
    private String name;
    private String courseNum;
    private String uniqueRandomID;

    public Course(String numOfPoints, String grade, String name, String courseNum) {
        this.numOfPoints = numOfPoints;
        this.grade = grade;
        this.name = name;
        this.courseNum = courseNum;
        uniqueRandomID = UUID.randomUUID().toString();
    }


    public String getGrade() {
        return grade;
    }

    public String getNumOfPoints() {
        return numOfPoints;
    }

    public String getName() {
        return name;
    }

    public String getNum() {
        return courseNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        if (numOfPoints != null ? !numOfPoints.equals(course.numOfPoints) : course.numOfPoints != null)
            return false;
        if (grade != null ? !grade.equals(course.grade) : course.grade != null) return false;
        if (name != null ? !name.equals(course.name) : course.name != null) return false;
        if (courseNum != null ? !courseNum.equals(course.courseNum) : course.courseNum != null)
            return false;
        return !(uniqueRandomID != null ? !uniqueRandomID.equals(course.uniqueRandomID) : course.uniqueRandomID != null);

    }

    @Override
    public int hashCode() {
        int result = numOfPoints != null ? numOfPoints.hashCode() : 0;
        result = 31 * result + (grade != null ? grade.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (courseNum != null ? courseNum.hashCode() : 0);
        result = 31 * result + (uniqueRandomID != null ? uniqueRandomID.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Course{" +
                "numOfPoints='" + numOfPoints + '\'' +
                ", grade='" + grade + '\'' +
                ", name='" + name + '\'' +
                ", courseNum='" + courseNum + '\'' +
                '}';
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public void setNumOfPoints(String numOfPoints) {
        this.numOfPoints = numOfPoints;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCourseNum(String courseNum) {
        this.courseNum = courseNum;
    }

    public String encodeToString() {
        return numOfPoints + ENCODING_DELIM + grade + ENCODING_DELIM + name +
                ENCODING_DELIM + courseNum + ENCODING_DELIM + uniqueRandomID;
    }

    public static Course parseFromString(String encodedCourse) {
        if (encodedCourse == null || encodedCourse.length() == 0) {
            return null;
        }
        String[] courseInfo = encodedCourse.split(ENCODING_DELIM);
        Course parsedCourse = new Course(courseInfo[0], courseInfo[1], courseInfo[2], courseInfo[3]);
        parsedCourse.uniqueRandomID = courseInfo[4];
        return parsedCourse;
    }
}
