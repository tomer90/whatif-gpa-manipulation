package tomer.whatif.courses;

import android.util.Log;
import android.util.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import tomer.whatif.utils.ErrorConstants;

/**
 * Created by Tomer on 01-Nov-15.
 */
public class CoursesGetter {
    final static String TAG = "CoursesGetter";
    public static final String FAILED_LOGON_INDICATOR = "SecureLogon";
    public static final int TIMEOUT_MILLIS = 2000;
    public static final String user_agent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36";
    public static final String LOGIN_URL = "http://techmvs.technion.ac.il:100/cics/wmn/wmngrad?ORD=1";

    String user;
    String pass;

    public CoursesGetter(String user_, String pass_) {
        user = user_;
        pass = pass_;
    }

    /**
     * Returns the un parsed html containing the courses.
     *
     * @return the html response as a list of string
     */
    public Pair<Integer, String> get() {
        try {
            Pair<Integer, String> responseCodeAndURL = getRedirectedLoginURL(LOGIN_URL);
            Integer responseCode = responseCodeAndURL.first;
            if (responseCode != 200) {
                return new Pair<>(responseCode, null);
            }
            return doHttpPost(responseCodeAndURL.second, "function=signon&userid=" + user + "&password=" + pass);
        } catch (IOException e) {
            Log.e(TAG, "Failed to get retrieve courses from UG", e);
            return new Pair<>(ErrorConstants.UNKNOWN_ERROR_ERROR_CODE, null);
        }
    }

    private Pair<Integer, String> doHttpPost(String urlStr, String payload) throws IOException {
        if (payload == null || urlStr == null) {
            return null;
        }
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("User-Agent", user_agent);
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-Length", String.valueOf(payload.getBytes().length));
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setConnectTimeout(TIMEOUT_MILLIS);
        conn.connect();
        OutputStream outputStream = conn.getOutputStream();
        outputStream.write(payload.getBytes());
        outputStream.flush();
        outputStream.close();
        String responseLines = inputStreamToListOfString(conn.getInputStream());
        int responseCode = responseLines != null ? conn.getResponseCode() : ErrorConstants.WRONG_CREDENTIALS_ERROR_CODE;
        conn.disconnect();
        return new Pair<>(responseCode, responseLines);
    }

    private Pair<Integer, String> getRedirectedLoginURL(String loginUrl) throws IOException {
        URL url = new URL(loginUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("User-Agent", user_agent);
        conn.connect();
        int responseCode = conn.getResponseCode();   // This call is only needed so
                                                     // "getURL" method will return to the redirected url
                                                     // and not the original one
        conn.disconnect();
        return new Pair<>(responseCode, conn.getURL().toString());
    }

    public static String inputStreamToListOfString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-8"));
        StringBuilder linesSB = new StringBuilder();
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
            if (line.contains(FAILED_LOGON_INDICATOR)) {
                return null;
            }
            linesSB.append(line);
        }
        reader.close();
        inputStream.close();
        return linesSB.toString();
    }
}
