package tomer.whatif.courses;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import tomer.whatif.utils.Utils;

/**
 * The list contains entries which are either courses or semesters summaries (points, avg etc...).
 * Example of how this list will look:
 *      - course1 who is part of semester1
 *      - course2 who is part of semester1
 *      - ...
 *      - semester1 summary
 *      - course1 who is part of semester2
 *      - course2 who is part of semester2
 *      - ...
 *      - semester2 summary
 *      - ...
 *
 * Created by Tomer on 30-Dec-15.
 */
public class CoursesArrayList extends ArrayList<Course> {
    private Map<String, Course> courseLastGlobalOccurrenceByCourseNum;
    private Map<String, Map<String, Course>> courseLastSemesterOccurrenceMapBySemesterNum;

    public CoursesArrayList() {
        super();
        courseLastGlobalOccurrenceByCourseNum = new HashMap<>();
        courseLastSemesterOccurrenceMapBySemesterNum = new HashMap<>();
    }

    private void buildLastOccurrencesMaps() {
        buildPerSemesterLastOccurrenceMap();
        buildGlobalLastOccurrenceMap();
    }

    private void buildPerSemesterLastOccurrenceMap() {
        Map<String, Map<String, Course>> courseLastOccurrenceMapBySemesterMap = new HashMap<>();
        Map<String, Course> courseLastOccurrenceByCourseNum = new HashMap<>();
        int semesterNum = 1;
        for (int index = 0; index < size(); index++) {
            Course course = get(index);
            if (CoursesParser.isSemesterSummaryPlaceHolder(course)) {
                course.setCourseNum(String.valueOf(semesterNum++));
                courseLastOccurrenceMapBySemesterMap.put(course.getNum(), courseLastOccurrenceByCourseNum);
                courseLastOccurrenceByCourseNum = new HashMap<>();
            } else if (Utils.isNumber(course.getGrade())
                    || "-".equals(course.getGrade())) {
                courseLastOccurrenceByCourseNum.put(course.getNum(), course);
            }
        }
        courseLastSemesterOccurrenceMapBySemesterNum = courseLastOccurrenceMapBySemesterMap;
    }

    private void buildGlobalLastOccurrenceMap() {
        Map<String, Course> globalLastOccurrenceMap = new HashMap<>();
        for (int index = 0; index < size(); index++) {
            Course c = get(index);
            globalLastOccurrenceMap.put(c.getNum(), c);
        }
        courseLastGlobalOccurrenceByCourseNum = globalLastOccurrenceMap;
    }

    private void updateLastGlobalOccurrence(String courseNum) {
        Course lastOccurrence = getGlobalLastOccurrenceOfCourseNum(courseNum);
        if (lastOccurrence != null) {
            courseLastGlobalOccurrenceByCourseNum.put(courseNum, lastOccurrence);
        }
    }

    private void updateLastSemesterOccurrence(String semesterNum, String courseNum) {
        Course lastOccurrence = getSemesterLastOccurrenceOfCourseNum(
                Integer.parseInt(semesterNum),
                courseNum);
        if (lastOccurrence != null) {
            if (courseLastSemesterOccurrenceMapBySemesterNum.get(semesterNum) == null) {
                courseLastSemesterOccurrenceMapBySemesterNum.put(semesterNum, new HashMap<String, Course>());
            }
            courseLastSemesterOccurrenceMapBySemesterNum.get(semesterNum).put(courseNum, lastOccurrence);
        }
    }

    @Override
    public boolean addAll(Collection<? extends Course> collection) {
        boolean result = super.addAll(collection);
        buildLastOccurrencesMaps();
        return result;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Course> collection) {
        boolean result = super.addAll(index, collection);
        buildLastOccurrencesMaps();
        return result;
    }

    @Override
    public Course remove(int index) {
        Course courseToRemove = get(index);
        String numOfSemesterCourseIsIn = getSemesterCourseIsIn(courseToRemove).getNum();
        Course removedCourse = super.remove(index);
        updateLastOccurrencesIfNeeded(numOfSemesterCourseIsIn, courseToRemove);
        return removedCourse;
    }

    @Override
    public boolean remove(Object object) {
        if (!contains(object)) {
            return true;
        }
        Course courseToRemove = (Course) object;
        String numOfSemesterCourseIsIn = getSemesterCourseIsIn(courseToRemove).getNum();
        boolean removeResult = super.remove(object);
        updateLastOccurrencesIfNeeded(numOfSemesterCourseIsIn, courseToRemove);
        return removeResult;
    }

    private void updateLastOccurrencesIfNeeded(String numOfSemesterCourseIsIn, Course course) {
        if (CoursesParser.isSemesterSummaryPlaceHolder(course)) {
            return;
        }
        if (isCourseLastOccurrenceInSemester(course, numOfSemesterCourseIsIn)) {
            updateLastSemesterOccurrence(numOfSemesterCourseIsIn, course.getNum());
        }
        if (isCourseLastGlobalOccurrence(course)) {
            updateLastGlobalOccurrence(course.getNum());
        }
    }

    public Course getGlobalLastOccurrenceOfCourseNum(String courseNum) {
        Course courseLastOccurrence = null;
        for (int i = 0; i < size(); i++) {
            Course course = get(i);
            if (courseNum.equals(course.getNum())) {
                courseLastOccurrence = course;
            }
        }
        return courseLastOccurrence;
    }

    public Course getSemesterLastOccurrenceOfCourseNum(int semesterNum, String courseNum) {
        Course lastOccurrence = null;
        int lastSemesterFound = 0;
        for (int i = 0; i < size(); i++) {
            Course course = get(i);
            if (CoursesParser.isSemesterSummaryPlaceHolder(course)) {
                lastSemesterFound = Integer.parseInt(course.getNum());
                continue;
            }
            if (lastSemesterFound == semesterNum - 1 && courseNum.equals(course.getNum())) {
                lastOccurrence = course;
            }
        }
        return lastOccurrence;
    }

    public Course getSemesterCourseIsIn(Course course) {
        for (int i = indexOf(course); i < size(); i++) {
            Course currCourse = get(i);
            if (CoursesParser.isSemesterSummaryPlaceHolder(currCourse)) {
                return currCourse;
            }
        }
        return null;
    }

    public Course getSemesterByNum(String semesterNum) {
        for (int i = 0; i < size(); i++) {
            Course course = get(i);
            if (CoursesParser.isSemesterSummaryPlaceHolder(course)
                    && course.getNum().equals(semesterNum)) {
                return course;
            }
        }
        return null;
    }

    public boolean isCourseLastOccurrenceInSemester(Course course, String semesterNum) {
        Map<String, Course> courseLastOccurrenceInSemesterByCourseNum = courseLastSemesterOccurrenceMapBySemesterNum.get(semesterNum);
        if (courseLastOccurrenceInSemesterByCourseNum == null) {
            return true;
        }
        Course lastOccurrence = courseLastOccurrenceInSemesterByCourseNum.get(course.getNum());
        return course.equals(lastOccurrence);
    }

    public boolean isCourseLastGlobalOccurrence(Course courseToRemove) {
        Course courseGlobalLastOccurrence = courseLastGlobalOccurrenceByCourseNum.get(courseToRemove.getNum());
        return courseGlobalLastOccurrence != null && courseGlobalLastOccurrence.equals(courseToRemove);
    }

    public Collection<Course> getCoursesInSemester(String semesterNum) {
        return courseLastSemesterOccurrenceMapBySemesterNum.get(semesterNum).values();
    }

    public Set<String> getSemesterNumbers() {
        return courseLastSemesterOccurrenceMapBySemesterNum.keySet();
    }

    public Collection<Course> getAllCoursesGlobalLastOccurrences() {
        return courseLastGlobalOccurrenceByCourseNum.values();
    }

    public void removeAllOccurrencesByCourseNum(String courseNum) {
        ArrayList<Course> instances = new ArrayList<>();
        for (int i = 0; i < size(); i++) {
            Course course = get(i);
            if (course.getNum().equals(courseNum)) {
                instances.add(course);
            }
        }
        removeAll(instances);
    }
}
