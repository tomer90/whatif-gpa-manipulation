package tomer.whatif.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import tomer.whatif.R;
import tomer.whatif.app.WhatIfApplication;
import tomer.whatif.courses.CoursesGetter;
import tomer.whatif.courses.CoursesParser;
import tomer.whatif.courses.Course;
import tomer.whatif.courses.CoursesArrayList;
import tomer.whatif.utils.CoursesDBWrapper;
import tomer.whatif.utils.ErrorConstants;
import tomer.whatif.utils.GPAAccumulator;
import tomer.whatif.utils.Utils;

public class GradeSheetEditingActivity extends AppCompatActivity {

    public static final String LOADING_GRADES_MESSAGE = "Getting grades...";
    public static final String LOADING_GRADES_TITLE = "Hold On!";
    public static final String EXEMPT_COURSE_GRADE = "דוקינ םע רוטפ";
    public static final String BINARY_PASS_COURSE_GRADE = "רבע";
    public static final String SEMESTER_GPA_ENTRY_NAME = "ממוצע סמסטר";

    private ListView gradesListView;
    private TextView averageTextView;
    private TextView pointsTextView;
    private double numOfPoints = 0.0;
    private GradesListArrayAdapter gradesListArrayAdapter;
    private GPAAccumulator globalGPAAccumulator;
    private Map<String, GPAAccumulator> semesterGPAAccumulatorBySemesterNum;
    private CoursesArrayList coursesList;
    private ProgressDialog loadingGradesProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grade_sheet_editing);
        gradesListView = (ListView) findViewById(R.id.grades_list_view);
        averageTextView = (TextView) findViewById(R.id.average_text_view);
        pointsTextView = (TextView) findViewById(R.id.points_text_view);
        globalGPAAccumulator = new GPAAccumulator();
        semesterGPAAccumulatorBySemesterNum = new HashMap<>();
        loadingGradesProgressDialog = ProgressDialog.show(this, LOADING_GRADES_TITLE, LOADING_GRADES_MESSAGE, true, false);
        String dataSource = getIntent().getExtras().getString(LoginActivity.INTENT_EXTRA_DATA_SOURCE);
        coursesList = new CoursesArrayList();
        new GetGradesAsync().execute(dataSource);

        gradesListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.hideSoftKeyboard(GradeSheetEditingActivity.this);
                GradeSheetEditingActivity.this.getCurrentFocus().clearFocus();
                return false;
            }
        });
    }

    /**
     * The function removes courses which has points but no grade (exempt courses)
     * and returns the some of points of these courses
     *
     * @param courses courses list to scan
     * @return the accumulated number of points of the courses removed.
     */
    private double removeExemptCourses(List<Course> courses) {
        double numOfPoints = 0.0;
        ArrayList<Course> coursesToRemove = new ArrayList<>();
        HashSet<String> courseNumbersToRemove = new HashSet<>();
        for (Course c : courses) {
            if (EXEMPT_COURSE_GRADE.equals(c.getGrade())) {
                numOfPoints += Double.parseDouble(c.getNumOfPoints());
                coursesToRemove.add(c);
                courseNumbersToRemove.add(c.getNum());
            } else if (BINARY_PASS_COURSE_GRADE.equals(c.getGrade())) {
                boolean foundCourseInstance = false;
                for (int index = courses.indexOf(c) + 1; index < courses.size(); index++) {
                    foundCourseInstance = foundCourseInstance || c.getNum().equals(courses.get(index).getNum());
                }
                if (!foundCourseInstance) {
                    numOfPoints += Double.parseDouble(c.getNumOfPoints());
                    coursesToRemove.add(c);
                    courseNumbersToRemove.add(c.getNum());
                }
            }
        }
        for (Course course : courses) {
            if (!coursesToRemove.contains(course)
                    && courseNumbersToRemove.contains(course.getNum())) {
                coursesToRemove.add(course);
            }
        }
        courses.removeAll(coursesToRemove);
        return numOfPoints;
    }

    private void updatePointsCountTextView() {
        pointsTextView.setText(String.valueOf(numOfPoints));
    }

    private void updateAverageTextView() {
        averageTextView.setText(globalGPAAccumulator.getFormattedCurrentAverage());
    }

    private void initSemesterSummaryPlaceHolders(Collection<String> semesterNumbers) {
        for (String semesterNum : semesterNumbers) {
            Course semesterPlaceHolder = coursesList.getSemesterByNum(semesterNum);
            Collection<Course> coursesInSemester = coursesList.getCoursesInSemester(semesterNum);
            GPAAccumulator semesterAverage = new GPAAccumulator();
            semesterAverage.addCoursesToAverage(coursesInSemester);
            semesterGPAAccumulatorBySemesterNum.put(semesterNum, semesterAverage);
            semesterPlaceHolder.setName(SEMESTER_GPA_ENTRY_NAME);
            semesterPlaceHolder.setGrade(semesterAverage.getFormattedCurrentAverage());
            semesterPlaceHolder.setNumOfPoints(semesterAverage.getNumOfPointsAsString());
        }
    }

    public void onClickRemoveIcon(View v) {
        int positionToRemove = gradesListView.getPositionForView(v);
        Course courseToRemove = gradesListArrayAdapter.getItem(positionToRemove);
        buildCourseRemoveAlertDialog(courseToRemove).show();
    }

    private void removeSpecificCourseInstance(Course courseToRemove) {
        Course semester = coursesList.getSemesterCourseIsIn(courseToRemove);
        // Have to check these before we remove the course because the last occurrences are updated
        boolean isCourseLastGlobalOccurrence = coursesList.isCourseLastGlobalOccurrence(courseToRemove);
        boolean isCourseLastOccurrenceInSemester = coursesList.isCourseLastOccurrenceInSemester(courseToRemove, semester.getNum());
        // On course removal
        coursesList.remove(courseToRemove);
        if (isCourseLastGlobalOccurrence) {
            removeCourseFromSemester(courseToRemove, semester);
            removeCourseFromGlobal(courseToRemove);
        } else if (isCourseLastOccurrenceInSemester) {
            removeCourseFromSemester(courseToRemove, semester);
        }
        gradesListArrayAdapter.notifyDataSetChanged();
    }

    private void removeAllInstancesOfCourse(Course courseToRemove) {
        Course courseLastGlobalOccurrence = coursesList.getGlobalLastOccurrenceOfCourseNum(courseToRemove.getNum());
        courseLastGlobalOccurrence = getCourseLastOccurrenceWithGrade(courseLastGlobalOccurrence.getNum());
        for (int index=0; index < coursesList.size(); index++) {
            Course c = coursesList.get(index);
            if (c.getNum().equals(courseToRemove.getNum())) {
                Course semesterCourseIsIn = coursesList.getSemesterCourseIsIn(c);
                if (coursesList.isCourseLastOccurrenceInSemester(c, semesterCourseIsIn.getNum())) {
                    removeCourseFromSemesterGPA(c, semesterCourseIsIn);
                }
            }
        }
        coursesList.removeAllOccurrencesByCourseNum(courseToRemove.getNum());
        removeCourseFromGlobalGPA(courseLastGlobalOccurrence);
        gradesListArrayAdapter.notifyDataSetChanged();
    }

    private AlertDialog buildCourseRemoveAlertDialog(final Course courseToRemove) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Remove all '" + courseToRemove.getName() + "', or just this one?");
        builder.setPositiveButton("All", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                removeAllInstancesOfCourse(courseToRemove);
            }
        });
        builder.setNegativeButton("Just This One", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                removeSpecificCourseInstance(courseToRemove);
            }
        });
        return builder.create();
    }

    private void removeCourseFromSemester(Course courseToRemove, Course semester) {
        int semesterNum = Integer.parseInt(semester.getNum());
        Course courseNewLastOccurrence = coursesList.getSemesterLastOccurrenceOfCourseNum(semesterNum, courseToRemove.getNum());
        removeCourseFromSemesterGPA(courseToRemove, semester);
        addCourseToSemesterGPA(courseNewLastOccurrence, semester);
    }

    private void removeCourseFromGlobal(Course courseToRemove) {
        removeCourseFromGlobalGPA(courseToRemove);
        Course courseNewLastOccurrence = coursesList.getGlobalLastOccurrenceOfCourseNum(courseToRemove.getNum());
        addCourseToGlobalGPA(courseNewLastOccurrence);
    }

    private void processCoursesList(List<Course> courses) {
        new UpdateDBWithCoursesAsync().execute(new ArrayList<>(courses));
        numOfPoints += removeExemptCourses(courses);
        coursesList.addAll(courses);
        gradesListArrayAdapter = new GradesListArrayAdapter(this, R.layout.grades_list_item, coursesList);
        gradesListView.setAdapter(gradesListArrayAdapter);
        initSemesterSummaryPlaceHolders(coursesList.getSemesterNumbers());
        globalGPAAccumulator.addCoursesToAverage(coursesList.getAllCoursesGlobalLastOccurrences());
        handleRepeatCourses(courses);
        numOfPoints += globalGPAAccumulator.getNumOfPoints();
        gradesListArrayAdapter.notifyDataSetChanged();
        updateAverageTextView();
        updatePointsCountTextView();
        loadingGradesProgressDialog.dismiss();
    }

    private void handleRepeatCourses(List<Course> courses) {
        for (Course c : coursesList.getAllCoursesGlobalLastOccurrences()){
            if ("-".equals(c.getGrade())){
                Course courseCurrentLastOccurrence = null;
                for (Course cc : courses){
                    if (c.getNum().equals(cc.getNum()) && !c.equals(cc)){
                        courseCurrentLastOccurrence = cc;
                    }
                }
                globalGPAAccumulator.addCoursesToAverage(Arrays.asList(courseCurrentLastOccurrence));
            }
        }
    }

    private void removeCourseFromSemesterGPA(Course course, Course semester) {
        updateSemesterGPAWithCourse(course, semester, Utils.Action.REMOVE);
    }

    private void addCourseToSemesterGPA(Course course, Course semester) {
        updateSemesterGPAWithCourse(course, semester, Utils.Action.ADD);
    }

    private void updateSemesterGPAWithCourse(Course course, Course semester, Utils.Action action) {
        if (course == null || semester == null) {
            return;
        }
        GPAAccumulator semesterGPAAccumulator = semesterGPAAccumulatorBySemesterNum.get(semester.getNum());
        switch (action) {
            case ADD: {
                semesterGPAAccumulator.addSingleCourseToAverage(course);
                break;
            }
            case REMOVE: {
                semesterGPAAccumulator.removeSingleCourseFromAverage(course);
                break;
            }
            default: break;
        }
        semester.setGrade(semesterGPAAccumulator.getFormattedCurrentAverage());
        semester.setNumOfPoints(semesterGPAAccumulator.getNumOfPointsAsString());
        gradesListArrayAdapter.updateSemesterViewIfVisible(semester);
    }

    private void removeCourseFromGlobalGPA(Course course) {
        updateGlobalGPAWithCourse(course, Utils.Action.REMOVE);
    }

    private void addCourseToGlobalGPA(Course course) {
        updateGlobalGPAWithCourse(course, Utils.Action.ADD);
    }

    private void updateGlobalGPAWithCourse(Course course, Utils.Action action) {
        if (course == null) {
            return;
        }
        switch (action) {
            case ADD: {
                globalGPAAccumulator.addSingleCourseToAverage(course);
                if (Utils.isNumber(course.getGrade())) {
                    numOfPoints += Double.parseDouble(course.getNumOfPoints());
                }
                break;
            }
            case REMOVE: {
                globalGPAAccumulator.removeSingleCourseFromAverage(course);
                if (Utils.isNumber(course.getGrade())) {
                    numOfPoints -= Double.parseDouble(course.getNumOfPoints());
                }
                break;
            }
            default: break;
        }
        updateAverageTextView();
        updatePointsCountTextView();
    }

    private Course getCourseLastOccurrenceWithGrade(String courseNum) {
        Course course = null;
        for (int position = 0; position < gradesListArrayAdapter.getCount(); position++){
            Course curr = gradesListArrayAdapter.getItem(position);
            if (curr.getNum().equals(courseNum) && Utils.isNumber(curr.getGrade())){
                course = curr;
            }
        }
        return course;
    }

    private class GradeChangeWatcher implements TextWatcher {

        EditText gradeEditTextWatched;
        boolean disabled;
        String textBeforeChange;

        public GradeChangeWatcher(EditText et) {
            gradeEditTextWatched = et;
            disabled = false;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            textBeforeChange = s.toString();
            if (disabled || s.toString().equals("-")) {
                return;
            }
            int coursePosition = gradesListView.getPositionForView((View) (gradeEditTextWatched.getParent()));
            Course course = gradesListArrayAdapter.getItem(coursePosition);
            course = getCourseLastOccurrenceWithGrade(course.getNum());
            Course semester = coursesList.getSemesterCourseIsIn(course);
            removeCourseFromGlobalGPA(course);
            if (coursesList.isCourseLastOccurrenceInSemester(course, semester.getNum())) {
                removeCourseFromSemesterGPA(course, semester);
                int lengthAfterChange = s.length() - count + after;
                if (lengthAfterChange == 0) {
                    course.setGrade("");
                }
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (disabled || s.length() == 0) {
                if (s.length() == 0 && !textBeforeChange.equals("-")){
                    int coursePosition = gradesListView.getPositionForView((View) (gradeEditTextWatched.getParent()));
                    Course course = gradesListArrayAdapter.getItem(coursePosition);
                    course = getCourseLastOccurrenceWithGrade(course.getNum());
                    addCourseToGlobalGPA(course);
                }
                return;
            }
            int coursePosition = gradesListView.getPositionForView((View) (gradeEditTextWatched.getParent()));
            Course course = gradesListArrayAdapter.getItem(coursePosition);
            course.setGrade(gradeEditTextWatched.getText().toString());
            Course semester = coursesList.getSemesterCourseIsIn(course);
            if (coursesList.isCourseLastGlobalOccurrence(course)) {
                addCourseToGlobalGPA(course);
            }
            if (coursesList.isCourseLastOccurrenceInSemester(course, semester.getNum())) {
                addCourseToSemesterGPA(course, semester);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }

        public void disable() {
            disabled = true;
        }

        public void enable() {
            disabled = false;
        }

    }

    private class GetGradesAsync extends AsyncTask<String, Void, List<Course>> {

        @Override
        protected List<Course> doInBackground(String... params) {
            String dataSource = params[0];
            switch(dataSource) {
                case LoginActivity.INTENT_EXTRA_DATA_SOURCE_UG: {
                    return GetListOfCoursesFromUG();
                }
                case LoginActivity.INTENT_EXTRA_DATA_SOURCE_DB: {
                    return loadCoursesFromDB();
                }
                default: return null;
            }
        }

        @Override
        protected void onPostExecute(List<Course> courses) {
            super.onPostExecute(courses);
            if (courses.size() == 0) {
                Intent resultIntent = new Intent();
                setResult(ErrorConstants.NO_COURSES_FOUND_ERROR_CODE, resultIntent);
                finish();
                return;
            } else {
                processCoursesList(courses);
            }
        }

        private ArrayList<Course> GetListOfCoursesFromUG() {
            String user = getIntent().getStringExtra(LoginActivity.LOGIN_USERNAME_INTENT_EXTRA);
            String pass = getIntent().getStringExtra(LoginActivity.LOGIN_PASS_INTENT_EXTRA);
            Pair<Integer, String> response = new CoursesGetter(user, pass).get();
            int responseCode = response.first;
            String responseLines = response.second;
            if (responseCode != 200 || responseLines == null) {
                Intent resultIntent = new Intent();
                setResult(ErrorConstants.UNABLE_TO_REACH_UG_ERROR_CODE, resultIntent);
                finish();
                return new ArrayList<>();
            }
            return new CoursesParser().parse(responseLines);
        }

        private List<Course> loadCoursesFromDB() {
            CoursesDBWrapper coursesDBWrapper = ((WhatIfApplication) getApplication()).getCoursesDBWrapper();
            List<Course> courses = coursesDBWrapper.getCoursesList();
            coursesDBWrapper.close();
            return courses;
        }
    }

    private class UpdateDBWithCoursesAsync extends AsyncTask<Object, Void, Void> {

        @Override
        protected Void doInBackground(Object... params) {
            if (params == null) {
                return null;
            }
            List<Course> courses = (List<Course>) params[0];
            CoursesDBWrapper coursesDBWrapper = ((WhatIfApplication) getApplication()).getCoursesDBWrapper();
            coursesDBWrapper.storeCoursesList(courses);
            coursesDBWrapper.close();
            return null;
        }
    }

    private class GradesListArrayAdapter extends ArrayAdapter<Course> {
        HashMap<View, GradeChangeWatcher> gradeChangeWatcherByGradeView;

        public GradesListArrayAdapter(GradeSheetEditingActivity activity,
                                      int gradesListItemLayoutID,
                                      List<Course> courses) {
            super(activity, gradesListItemLayoutID, courses);
            gradeChangeWatcherByGradeView = new HashMap<>();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Course course = getItem(position);
            boolean isSemesterAveragePlaceHolder = CoursesParser.isSemesterSummaryPlaceHolder(course);
            if (convertView == null || !isViewMatchingItem(convertView, isSemesterAveragePlaceHolder)) {
                int layoutID = isSemesterAveragePlaceHolder ?
                    R.layout.semester_summary_item : R.layout.grades_list_item;
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(layoutID, null);
                if (!isSemesterAveragePlaceHolder) {
                    EditText courseGradeEditText = (EditText) convertView.findViewById(R.id.grade_text_view);
                    gradeChangeWatcherByGradeView.put(courseGradeEditText, new GradeChangeWatcher(courseGradeEditText));
                    courseGradeEditText.addTextChangedListener(gradeChangeWatcherByGradeView.get(courseGradeEditText));
                }
            }
            initView(convertView, course, isSemesterAveragePlaceHolder);
            return convertView;
        }

        private void initView(View convertView, Course course, boolean isSemesterAveragePlaceHolder) {
            TextView courseNumTextView = (TextView) convertView.findViewById(R.id.course_num_text_view);
            TextView courseNameTextView = (TextView) convertView.findViewById(R.id.course_name_text_view);
            TextView courseNumOfPointsTextView = (TextView) convertView.findViewById(R.id.points_num_text_view);
            TextView courseGradeTextView = (TextView) convertView.findViewById(R.id.grade_text_view);
            courseNameTextView.setText(course.getName());
            courseNumOfPointsTextView.setText(course.getNumOfPoints());
            if (!isSemesterAveragePlaceHolder) {
                courseNumTextView.setText(course.getNum().split("-")[0]);
                gradeChangeWatcherByGradeView.get(courseGradeTextView).disable();
                courseGradeTextView.setText(course.getGrade());
                gradeChangeWatcherByGradeView.get(courseGradeTextView).enable();
            } else {
                courseGradeTextView.setText(Utils.formatAverage(course.getGrade()));
            }
        }

        private boolean isViewMatchingItem(View view, boolean isSemesterAveragePlaceHolder) {
            TextView courseNumTextView = (TextView) view.findViewById(R.id.course_num_text_view);
            return (isSemesterAveragePlaceHolder && courseNumTextView == null)
                    || (!isSemesterAveragePlaceHolder && courseNumTextView != null);
        }

        public void updateSemesterViewIfVisible(Course semester) {
            int firstVisiblePosition = gradesListView.getFirstVisiblePosition();
            int lastVisiblePosition = gradesListView.getLastVisiblePosition();
            for (int index = firstVisiblePosition; index <= lastVisiblePosition; index++){
                if (gradesListView.getItemAtPosition(index).equals(semester)) {
                    View semesterView = gradesListView.getChildAt(index - firstVisiblePosition);
                    TextView gradeTextView = (TextView) semesterView.findViewById(R.id.grade_text_view);
                    TextView numOfPointsTextView = (TextView) semesterView.findViewById(R.id.points_num_text_view);
                    gradeTextView.setText(semester.getGrade());
                    numOfPointsTextView.setText(semester.getNumOfPoints());
                    return;
                }
            }
        }

        @Override
        public void notifyDataSetChanged() {
            disableAllTextWatchers();
            super.notifyDataSetChanged();
            enableAllTextWatchers();
        }

        public void disableAllTextWatchers() {
            for (GradeChangeWatcher gradeChangeWatcher : gradeChangeWatcherByGradeView.values()) {
                gradeChangeWatcher.disable();
            }
        }

        public void enableAllTextWatchers() {
            for (GradeChangeWatcher gradeChangeWatcher : gradeChangeWatcherByGradeView.values()) {
                gradeChangeWatcher.enable();
            }
        }
    }
}
