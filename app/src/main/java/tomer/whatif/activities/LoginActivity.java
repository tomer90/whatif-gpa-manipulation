package tomer.whatif.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import tomer.whatif.R;
import tomer.whatif.app.WhatIfApplication;
import tomer.whatif.utils.ErrorConstants;
import tomer.whatif.utils.Utils;

public class LoginActivity extends AppCompatActivity {
    public final static String LOGIN_USERNAME_INTENT_EXTRA = "intent_extra_username";
    public final static String LOGIN_PASS_INTENT_EXTRA = "intent_extra_pass";
    public final static String INTENT_EXTRA_DATA_SOURCE = "dataSource";
    public final static String INTENT_EXTRA_DATA_SOURCE_DB = "DB";
    public final static String INTENT_EXTRA_DATA_SOURCE_UG = "UG";

    private final static String PREFERENCE_REMEMBER_CREDENTIALS_CHECKBOX = "pref_remember_checkbox";

    private EditText id;
    private EditText password;
    private CheckBox remember_credentials;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        id = (EditText) findViewById(R.id.entered_id);
        password = (EditText) findViewById(R.id.entered_password);
        remember_credentials = (CheckBox) findViewById(R.id.remember_credentials_checkBox);
        loadCheckBoxPreference(PREFERENCE_REMEMBER_CREDENTIALS_CHECKBOX, remember_credentials);
    }

    @Override
    protected void onStart() {
        super.onStart();
        WhatIfApplication applicationContext = (WhatIfApplication) getApplicationContext();
        String username = applicationContext.getStringValueFromSharedPrefs(WhatIfApplication.SHARED_PREFS_USERNAME_KEY);
        String pass = applicationContext.getStringValueFromSharedPrefs(WhatIfApplication.SHARED_PREFS_PASSWORD_KEY);
        if (username != null && pass != null){
            id.setText(username);
            password.setText(pass);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String messageToDisplay = resultCode != 0 ? ErrorConstants.UNKNOWN_ERROR_MESSAGE : null;
        switch (resultCode) {
            case ErrorConstants.UNABLE_TO_REACH_UG_ERROR_CODE:
                messageToDisplay = ErrorConstants.UNABLE_TO_REACH_UG_ERROR_MESSAGE;
                break;
            case ErrorConstants.WRONG_CREDENTIALS_ERROR_CODE:
                messageToDisplay = ErrorConstants.WRONG_CREDENTIALS_ERROR_MESSAGE;
                break;
            case ErrorConstants.NO_COURSES_FOUND_ERROR_CODE:
                messageToDisplay = ErrorConstants.NO_COURSES_FOUND_ERROR_MESSAGE;
                break;
            default: break;
        }
        if (messageToDisplay != null) {
            Toast.makeText(this, messageToDisplay, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_contact_me) {
            Utils.sendEmail(new String[]{"tomerr90@gmail.com"}, "[WhatIf] User experience", this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickLoginButton(View v) {
        if (remember_credentials.isChecked()) {
            saveCredentials();
        }
        Intent gradesActivityIntent = new Intent(this, GradeSheetEditingActivity.class);
        gradesActivityIntent.putExtra(LOGIN_USERNAME_INTENT_EXTRA, id.getText().toString());
        gradesActivityIntent.putExtra(LOGIN_PASS_INTENT_EXTRA, password.getText().toString());
        gradesActivityIntent.putExtra(INTENT_EXTRA_DATA_SOURCE, INTENT_EXTRA_DATA_SOURCE_UG);
        startActivityForResult(gradesActivityIntent, 0);
    }


    public void onClickRememberCredentialsCheckbox(View v) {
        CheckBox rememberCredentialsCheckBox = (CheckBox) v;
        final boolean checked = rememberCredentialsCheckBox.isChecked();
        saveCheckBoxPreference(PREFERENCE_REMEMBER_CREDENTIALS_CHECKBOX, checked);
        if (!checked) {
            WhatIfApplication applicationContext = (WhatIfApplication) getApplicationContext();
            applicationContext.storeStringValueInSharedPrefs(WhatIfApplication.SHARED_PREFS_USERNAME_KEY, null);
            applicationContext.storeStringValueInSharedPrefs(WhatIfApplication.SHARED_PREFS_PASSWORD_KEY, null);
        } else {
            saveCredentials();
        }
    }

    private void saveCredentials() {
        final String entered_username = id.getText().toString();
        final String entered_password = password.getText().toString();
        WhatIfApplication applicationContext = (WhatIfApplication) getApplicationContext();
        applicationContext.storeStringValueInSharedPrefs(WhatIfApplication.SHARED_PREFS_USERNAME_KEY, entered_username);
        applicationContext.storeStringValueInSharedPrefs(WhatIfApplication.SHARED_PREFS_PASSWORD_KEY, entered_password);
    }

    private void saveCheckBoxPreference(String prefKey, boolean value) {
        SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(prefKey, value);
        editor.apply();
    }

    private void loadCheckBoxPreference(String prefKey, CheckBox cb) {
        SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
        cb.setChecked(prefs.getBoolean(prefKey, false));
    }

    public void onClickShowExistingGradesButton(View v) {
        Intent gradesActivityIntent = new Intent(this, GradeSheetEditingActivity.class);
        gradesActivityIntent.putExtra(INTENT_EXTRA_DATA_SOURCE, INTENT_EXTRA_DATA_SOURCE_DB);
        startActivityForResult(gradesActivityIntent, 0);
    }
}
